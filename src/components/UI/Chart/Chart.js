import classes from './Chart.module.css'

const Chart = (props) => {
    return (
        <div className={classes.chart}>
            <div className={classes.value} style={{width: props.width}} />
        </div>
    )
}

export default Chart
